from flask import Flask,render_template,request
from werkzeug import cached_property
import markdown 
from wtforms import Form, TextField, PasswordField, validators
from flask.ext.sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://me:abcdefgh@localhost:5432/db'
db = SQLAlchemy(app)


class User(db.Model):
	id=db.Column(db.Integer,primary_key=True)
	username = db.Column(db.String(80), unique=True)
	email=db.Column(db.String(80),unique=True)
	def __init__(self,ind,username,email):
		self.id=id
		self.username=username
		self.email=email
	def __repr__(self):
		return '<User %r>' % self.id  

		#""",% self.username ,% self.email"""	

class RegistrationForm(Form):
	id = TextField('identification number')
	username=TextField('Username')
	email = TextField('Email Address')
	password = PasswordField('Password', [validators.Required(),validators.EqualTo('confirm', message='Passwords must match')])
	confirm = PasswordField('Repeat Password')
   

@app.route('/logint', methods=['GET', 'POST'])
def reg():
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
    	user=User(form.id.data,form.username.data,form.email.data)
    	db.session.add(user)
    	db.session.commit()
    return render_template('register.html', form=form)

if __name__ == '__main__':
		app.run(port=8000, debug=True)
